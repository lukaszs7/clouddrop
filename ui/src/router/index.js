import Vue from 'vue'
import Router from 'vue-router'
import Hello from '@/components/Hello'
import Link from '@/components/Link'
import About from '@/components/About'
import New from '@/components/New'
import Manage from '@/components/Manage'

Vue.use(Router)

export default new Router({
  mode: 'history',
  hashbang: false,
  routes: [
    {
      path: '/about',
      name: 'About',
      component: About
    },
    {
      path: '/manage',
      name: 'Manage',
      component: Manage
    },
    {
      path: '/new',
      name: 'New',
      component: New,
      props: true
    },
    {
      path: '/new/:link',
      name: 'NewWithProp',
      component: New,
      props: true
    },
    {
      path: '/:link',
      name: "Link",
      component: Link,
      props: true
    },
    {
      path: '/',
      name: 'Home',
      component: About
    }
  ]
})
