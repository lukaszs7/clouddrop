var dataService = (function ($) {
    var apiUrl = "http://clouddrop.herokuapp.com";

    var createGetPromise = function (endpoint, options) {
        return new Promise((success, failure) => {
            $.get(apiUrl + endpoint).then(success).fail(failure);
        });
    };

    var createPostPromise = function (endpoint, data, options) {
        options = options || {};

        return new Promise((success, failure) => {
            $.ajax($.extend({
                type: "POST",
                url: apiUrl + endpoint,
                contentType: "application/json",
                dataType: 'json',
                async: false,
                data: JSON.stringify(data),
                success: function (data) {
                    success(data);
                },
                error: function (data) {
                    failure(data);
                }
            }, options));
        });
    };

    return {
        checkLink: (link) => {
            return createGetPromise("/link/" + link);
        },
        checkUser: (identifier, password) => {
            return createPostPromise("/management/user", {
                "identifier": identifier,
                "deletePassword": password
            });
        },
        getDropboxUri: (link, password) => {
            return createPostPromise("/dropbox/uri", {
                "linkIdentifier": link,
                "password": password
            }, {
                "dataType": "text"
            });
        },
        registerLink: (link, dropboxUri, password, managementPassword) => {
            var requestObject = {
                "deletePassword": managementPassword,
                "dropboxUri": dropboxUri,
                "identifier": link,
                "password": password
            };

            return createPostPromise("/register/link", requestObject, {
                "dataType": "text"
            });
        },
        unregisterLink: (link, managementPassword) => {
            var requestObject = {
                "deletePassword": managementPassword,
                "linkIdentifier": link
            };

            return createPostPromise("/unregister/link", requestObject, {
                "dataType": "text"
            });
        },
        changeDropboxUri: (identifier, managementPassword, newUri) => {
            var requestObject = {
                "deletePassword": managementPassword,
                "dropboxURI": newUri,
                "identifier": identifier,
            };

            return createPostPromise("/management/uri", requestObject, {
                "dataType": "text"
            });
        },
        changePassword: (identifier, managementPassword, newPassword) => {
            var requestObject = {
                "deletePassword": managementPassword,
                "password": newPassword,
                "identifier": identifier,
            };

            return createPostPromise("/management/password", requestObject, {
                "dataType": "text"
            });            
        }
    }
})(jQuery);

export default dataService;