package com.cloud.model;

/**
 * Created by Tomek on 24.06.2017.
 */
public class ChangeDropboxURIRequest {

    private String identifier;
    private String deletePassword;
    private String dropboxURI;


    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getDeletePassword() {
        return deletePassword;
    }

    public void setDeletePassword(String deletePassword) {
        this.deletePassword = deletePassword;
    }

    public String getDropboxURI() {
        return dropboxURI;
    }

    public void setDropboxURI(String dropboxURI) {
        this.dropboxURI = dropboxURI;
    }
}
