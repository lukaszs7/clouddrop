package com.cloud.model;

/**
 * Created by Łukasz on 24.06.2017.
 */
public class RegisterLinkRequest {
    private String identifier;
    private String dropboxUri;
    private String password;
    private String deletePassword;

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getDropboxUri() {
        return dropboxUri;
    }

    public void setDropboxUri(String dropboxUri) {
        this.dropboxUri = dropboxUri;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDeletePassword() {
        return deletePassword;
    }

    public void setDeletePassword(String deletePassword) {
        this.deletePassword = deletePassword;
    }
}
