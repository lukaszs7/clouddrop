package com.cloud.model;

/**
 * Created by Tomek on 24.06.2017.
 */
public class CheckUserRequest {

    private String deletePassword;
    private String identifier;

    public String getDeletePassword() {
        return deletePassword;
    }

    public void setDeletePassword(String deletePassword) {
        this.deletePassword = deletePassword;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }
}
