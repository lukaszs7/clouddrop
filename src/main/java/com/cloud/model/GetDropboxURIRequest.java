package com.cloud.model;

/**
 * Created by Łukasz on 24.06.2017.
 */
public class GetDropboxURIRequest {
    private String linkIdentifier;
    private String password;

    public String getLinkIdentifier() {
        return linkIdentifier;
    }

    public void setLinkIdentifier(String linkIdentifier) {
        this.linkIdentifier = linkIdentifier;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
