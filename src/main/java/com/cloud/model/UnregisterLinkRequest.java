package com.cloud.model;

/**
 * Created by Tomek on 24.06.2017.
 */
public class UnregisterLinkRequest {

    private String linkIdentifier;

    private String deletePassword;


    public String getLinkIdentifier() {
        return linkIdentifier;
    }

    public void setLinkIdentifier(String linkIdentifier) {
        this.linkIdentifier = linkIdentifier;
    }

    public String getDeletePassword() {
        return deletePassword;
    }

    public void setDeletePassword(String deletePassword) {
        this.deletePassword = deletePassword;
    }
}
