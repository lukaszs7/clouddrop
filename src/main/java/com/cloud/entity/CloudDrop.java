package com.cloud.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Tomek on 24.06.2017.
 */
@Entity
@Table(schema = "clouddrop")
public class CloudDrop implements Serializable {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private Date creationDate;

    @Column
    private Date lastVisitDate;

    @Column(length = 1000, unique = true)
    private String link;

    @Column(length = 1000)
    private String linkPassword;

    @Column(length = 1000)
    private String deletePassword;

    @Column(length = 1000)
    private String dropboxUri;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getLastVisitDate() {
        return lastVisitDate;
    }

    public void setLastVisitDate(Date lastVisitDate) {
        this.lastVisitDate = lastVisitDate;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getLinkPassword() {
        return linkPassword;
    }

    public void setLinkPassword(String linkPassword) {
        this.linkPassword = linkPassword;
    }

    public String getDeletePassword() {
        return deletePassword;
    }

    public void setDeletePassword(String deletePassword) {
        this.deletePassword = deletePassword;
    }

    public String getDropboxUri() {
        return dropboxUri;
    }

    public void setDropboxUri(String dropboxUri) {
        this.dropboxUri = dropboxUri;
    }
}
