package com.cloud.service;

import com.cloud.entity.CloudDrop;
import com.cloud.model.ChangeDropboxURIRequest;
import com.cloud.repository.CloudDropRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * Created by Tomek on 24.06.2017.
 */
@Service
public class ChangeDropboxURIService {

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    private CloudDropRepository cloudDropRepository;

    public boolean changeDropboxURI(ChangeDropboxURIRequest changeDropboxURIRequest) {
        CloudDrop cloudDrop = cloudDropRepository.findByLink(changeDropboxURIRequest.getIdentifier());
        if(cloudDrop!=null){
            if(checkDeletePassword(changeDropboxURIRequest.getDeletePassword(),cloudDrop.getDeletePassword())){
                cloudDrop.setDropboxUri(changeDropboxURIRequest.getDropboxURI());
                cloudDropRepository.save(cloudDrop);
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }

    }

    private boolean checkDeletePassword(String providedPassword, String encodedPassword){

        return passwordEncoder.matches(providedPassword,encodedPassword);
    }
}
