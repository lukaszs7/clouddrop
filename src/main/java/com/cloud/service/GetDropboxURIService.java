package com.cloud.service;

import com.cloud.entity.CloudDrop;
import com.cloud.model.GetDropboxURIRequest;
import com.cloud.repository.CloudDropRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * Created by Łukasz on 24.06.2017.
 */

@Service
public class GetDropboxURIService {

    @Autowired
    private CloudDropRepository cloudDropRepository;

    @Autowired
    private PasswordEncoder encoder;

    public String getDropboxUri(GetDropboxURIRequest getDropboxURIRequest) {
        CloudDrop cloudDrop = cloudDropRepository.findByLink(getDropboxURIRequest.getLinkIdentifier());
        if(cloudDrop == null) {
            return null;
        }

        boolean passwordCorrect = checkPassword(getDropboxURIRequest.getPassword(), cloudDrop.getLinkPassword());
        if(passwordCorrect) {
            return cloudDrop.getDropboxUri();
        } else {
            return null;
        }
    }

    private boolean checkPassword(String providedPassword, String encodedPassword) {
        return encoder.matches(providedPassword, encodedPassword);
    }
}
