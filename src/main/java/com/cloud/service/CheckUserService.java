package com.cloud.service;

import com.cloud.entity.CloudDrop;
import com.cloud.model.CheckUserRequest;
import com.cloud.repository.CloudDropRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * Created by Tomek on 24.06.2017.
 */

@Service
public class CheckUserService {

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    CloudDropRepository cloudDropRepository;


    public boolean checkUser(CheckUserRequest checkUserRequest) {
        CloudDrop cloudDrop = cloudDropRepository.findByLink(checkUserRequest.getIdentifier());
        if(cloudDrop!=null){
            if(checkDeletePassword(checkUserRequest.getDeletePassword(),cloudDrop.getDeletePassword())){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }


    private boolean checkDeletePassword(String providedPassword, String encodedPassword){
        return passwordEncoder.matches(providedPassword,encodedPassword);
    }
}
