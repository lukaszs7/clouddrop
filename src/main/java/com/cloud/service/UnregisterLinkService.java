package com.cloud.service;

import com.cloud.entity.CloudDrop;
import com.cloud.model.UnregisterLinkRequest;
import com.cloud.repository.CloudDropRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * Created by Tomek on 24.06.2017.
 */

@Service
public class UnregisterLinkService {

    @Autowired
    private CloudDropRepository cloudDropRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;


    public boolean unregisterLink(UnregisterLinkRequest unregisterLinkRequest){
        CloudDrop cloudDrop = cloudDropRepository.findByLink(unregisterLinkRequest.getLinkIdentifier());

        if(cloudDrop!=null){
            if(checkDeletePassword(unregisterLinkRequest.getDeletePassword(),cloudDrop.getDeletePassword())){
                cloudDropRepository.delete(cloudDrop);
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }

    }

    private boolean checkDeletePassword(String providedPassword, String encodedPassword){

        return passwordEncoder.matches(providedPassword,encodedPassword);
    }

}
