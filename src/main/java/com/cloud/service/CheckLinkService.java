package com.cloud.service;

import com.cloud.repository.CloudDropRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Łukasz on 24.06.2017.
 */
@Service
public class CheckLinkService {
    @Autowired
    private CloudDropRepository cloudDropRepository;

    public boolean checkLink(String identifier) {
        return cloudDropRepository.findByLink(identifier) != null;
    }
}
