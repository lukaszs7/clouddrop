package com.cloud.service;

import com.cloud.entity.CloudDrop;
import com.cloud.model.RegisterLinkRequest;
import com.cloud.repository.CloudDropRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * Created by Łukasz on 24.06.2017.
 */

@Service
public class RegisterLinkService {
    @Autowired
    private CloudDropRepository cloudDropRepository;

    @Autowired
    private PasswordEncoder encoder;

    public boolean registerLink(RegisterLinkRequest registerLinkRequest) {
        boolean correctLink = checkLinkRegex(registerLinkRequest.getIdentifier())&&checkKeyWords(registerLinkRequest.getIdentifier());
        if (correctLink) {
            boolean exists = checkIfLinkExists(registerLinkRequest.getIdentifier());
            if (exists) {
                return false;
            } else {
                CloudDrop cloudDrop = createCloudDropEntry(registerLinkRequest);
                cloudDropRepository.save(cloudDrop);
                return true;
            }
        } else {
            return false;
        }
    }

    private boolean checkLinkRegex(String identifier) {
        return identifier.matches("[a-zA-Z0-9]+");
    }

    private boolean checkKeyWords(String identifier) {
        return !identifier.equals("about")
                && !identifier.equals("home")
                && !identifier.equals("manage")
                && !identifier.equals("new");
    }


    private CloudDrop createCloudDropEntry(RegisterLinkRequest registerLinkRequest) {
        CloudDrop cloudDrop = new CloudDrop();
        cloudDrop.setCreationDate(new Date());
        cloudDrop.setLink(registerLinkRequest.getIdentifier());
        cloudDrop.setDropboxUri(registerLinkRequest.getDropboxUri());
        cloudDrop.setLinkPassword(encoder.encode(registerLinkRequest.getPassword()));
        cloudDrop.setDeletePassword(encoder.encode(registerLinkRequest.getDeletePassword()));
        return cloudDrop;
    }

    private boolean checkIfLinkExists(String identifier) {
        return cloudDropRepository.findByLink(identifier) != null;
    }
}
