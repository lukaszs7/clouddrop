package com.cloud.service;

import com.cloud.entity.CloudDrop;
import com.cloud.model.ChangePasswordRequest;
import com.cloud.repository.CloudDropRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * Created by Tomek on 24.06.2017.
 */
@Service
public class ChangePasswordService {

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    private CloudDropRepository cloudDropRepository;


    public boolean changePassword(ChangePasswordRequest changePasswordRequest) {
        CloudDrop cloudDrop = cloudDropRepository.findByLink(changePasswordRequest.getIdentifier());
        if(cloudDrop!=null){
            if(checkDeletePassword(changePasswordRequest.getDeletePassword(),cloudDrop.getDeletePassword())){
                cloudDrop.setLinkPassword(passwordEncoder.encode(changePasswordRequest.getPassword()));
                cloudDropRepository.save(cloudDrop);
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }


    private boolean checkDeletePassword(String providedPassword, String encodedPassword){
        return passwordEncoder.matches(providedPassword,encodedPassword);
    }
}
