package com.cloud.repository;

import com.cloud.entity.CloudDrop;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * Created by Łukasz on 24.06.2017.
 */

public interface CloudDropRepository extends JpaRepository<CloudDrop, Long> {
    CloudDrop findByLink(String identifier);
}
