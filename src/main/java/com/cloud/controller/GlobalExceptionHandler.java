package com.cloud.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * Created by Łukasz on 24.06.2017.
 */

@ControllerAdvice
public class GlobalExceptionHandler {
    @ExceptionHandler(Throwable.class)
    public ResponseEntity<String> exceptionHandler(Throwable e) {
        e.printStackTrace();
        return new ResponseEntity<>("Wewnętrzny błąd serwera", HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
