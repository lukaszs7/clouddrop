package com.cloud.controller;

import com.cloud.model.*;
import com.cloud.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Łukasz on 24.06.2017.
 */
@RestController
public class CloudDropController {

    @Autowired
    private RegisterLinkService registerLinkService;

    @Autowired
    private UnregisterLinkService unregisterLinkService;

    @Autowired
    private CheckLinkService checkLinkService;

    @Autowired
    private GetDropboxURIService getDropboxURIService;

    @Autowired
    private ChangeDropboxURIService changeDropboxURIService;

    @Autowired
    private CheckUserService checkUserService;

    @Autowired
    ChangePasswordService changePasswordService;

    @RequestMapping(value = "/register/link", method = RequestMethod.POST)
    public ResponseEntity<String> registerLink(@RequestBody RegisterLinkRequest registerLinkRequest) {
        registerLinkRequest.setIdentifier(registerLinkRequest.getIdentifier().toLowerCase());
        boolean correct = registerLinkService.registerLink(registerLinkRequest);
        if (correct) {
            return new ResponseEntity<>("Correctly registered link", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Incorrectly registered link", HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/unregister/link", method = RequestMethod.POST)
    public ResponseEntity<String> unregisterLink(@RequestBody UnregisterLinkRequest unregisterLinkRequest) {
        boolean correct = unregisterLinkService.unregisterLink(unregisterLinkRequest);
        if (correct) {
            return new ResponseEntity<>("Correctly deleted link", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Incorrectly deleted link", HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/link/{linkId}", method = RequestMethod.GET)
    public ResponseEntity<Boolean> checkLink(@PathVariable String linkId) {
        boolean exists = checkLinkService.checkLink(linkId.toLowerCase());
        return new ResponseEntity<>(exists, HttpStatus.OK);
    }

    @RequestMapping(value = "/dropbox/uri", method = RequestMethod.POST)
    public ResponseEntity<String> getDropboxURI(@RequestBody GetDropboxURIRequest getDropboxURIRequest) {
        String dropboxUri = getDropboxURIService.getDropboxUri(getDropboxURIRequest);
        if (dropboxUri != null) {
            return new ResponseEntity<>(dropboxUri, HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Cannot get uri", HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/management/password", method = RequestMethod.POST)
    public ResponseEntity<String> changePassword(@RequestBody ChangePasswordRequest changePasswordRequest) {
        boolean changed = changePasswordService.changePassword(changePasswordRequest);
        if (changed) {
            return new ResponseEntity<>("Password has been changed", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Password cannot be changed", HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/management/uri", method = RequestMethod.POST)
    public ResponseEntity<String> changeDropboxURI(@RequestBody ChangeDropboxURIRequest changeDropboxURIRequest) {
        boolean changed = changeDropboxURIService.changeDropboxURI(changeDropboxURIRequest);
        if (changed) {
            return new ResponseEntity<>("Uri has been changed", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Uri cannot be changed", HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/management/user", method = RequestMethod.POST)
    public ResponseEntity<Boolean> checkUser(@RequestBody CheckUserRequest checkUserRequest) {
        boolean exists = checkUserService.checkUser(checkUserRequest);
            return new ResponseEntity<>(exists, HttpStatus.OK);
        }

}
